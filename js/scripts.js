// Global Variables
var $messages;
var $devName;
var $appName;
var $temp;
var $reconBtn;
var $disconBtn;

$(document).ready(function() {
    console.log(new Date()); //log the date
    $messages = $("#messages");
    $temp = $("#temp");
    $reconBtn = $("#reconButton");
    $disconBtn = $("#disconButton");
    $devName = $("#deviceName");
    $appName = $("#applicationName");
});

//Create a new Client object with your broker's hostname, port and your own clientId
var wsbroker = "iot.op-bit.nz";  //mqtt websocket broker
var wsport = 1884 // port for above
var clientID = "IoTUser_" + parseInt(Math.random() * 100, 10);
var client = new Paho.MQTT.Client(wsbroker, wsport, clientID);

// Set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

var options = {

    //connection attempt timeout in seconds
    timeout: 3,

    //Gets Called if the connection has successfully been established
    onSuccess: function () {
        $messages.append('<span>Connected</span><br/>');
        console.log("Connected to MQTT Server");
        $reconBtn.attr("disabled", "disabled");
        onConnect();
    },

    //Gets Called if the connection could not be established
    onFailure: function () {
        $messages.append('<span>Failed to Connect</span><br/>');
        console.log("Failed to Connect to MQTT Server");
        
    }
};

//Attempt to connect
client.connect(options);

function reconnect() {
    $messages.append('<span>Reconnecting</span><br/>');
    client.connect(options);
}

// Called when the client connects
function onConnect() {
    // Fetch the MQTT topic

    $messages.append('<span>Connecting to: ' + wsbroker + ' on port: ' + wsport + '</span><br/>');
    $messages.append('<span>Using the following client value: ' + clientID + '</span><br/>');

    topic = 'application/2/node/2222222277771121/rx';

    // Print output for the user in the messages div
    $messages.append('<span>Subscribing to: ' + topic + '</span><br/>');

    // Subscribe to the requested topic
    client.subscribe(topic);

    // Enable the Disconnect Button
    $disconBtn.removeAttr("disabled");
}

// Called when the client loses its connection
function onConnectionLost(responseObject) {
    console.log("Connection Lost: " + responseObject.errorMessage);
    $messages.append('<span>ERROR: Connection lost</span><br/>');
    $devName.text("ERROR: Connection to MQTT Server Failed");
    if (responseObject.errorCode !== 0) {
        $messages.append('<span>ERROR: ' + responseObject.errorMessage + '</span><br/>');
        $appName.text("ERROR: " + responseObject.errorMessage);
        $temp.text("");
        }
    // Enable the Reconnect Button
    $reconBtn.removeAttr("disabled");
    // Disable the Disconnect Button
    $disconBtn.attr("disabled", "disabled");
}

// Called when a message arrives
function onMessageArrived(message) {
    console.log("onMessageArrived: " + message.payloadString);
    $messages.append('<span>Topic: ' + message.destinationName + '  | ' + message.payloadString + '</span><br/>');
    
    // Parse the payload message from the MQTT server into JSON format
    var sensorInfo = JSON.parse(message.payloadString);

    // Allocate variables from the JSON string to the website's info tab
    $devName.text("Device Name: " + sensorInfo.deviceName);
    $appName.text("Application Name: " + sensorInfo.applicationName);
    $decryptedInfo = decryptInfo(sensorInfo.data);
    $deviceInfo = $decryptedInfo.split(",");
    $deviceInfo[0] = $deviceInfo[0].replace("T", "Temperature");
    $deviceInfo[1] = $deviceInfo[1].replace("H", "Humidity");
    $deviceInfo[2] = $deviceInfo[2].replace("B", "Battery");
    $temp.text("Device Info: ");
    $temp.append("</br><span>" + $deviceInfo[0] +"</span></br>");
    $temp.append("<span>" + $deviceInfo[1] +"</span></br>");
    $temp.append("<span>" + $deviceInfo[2] +"</span></br>");
    

}

// Called when the disconnection button is pressed
function startDisconnect() {
    client.disconnect();
    $messages.append('<span>Disconnected</span><br/>');
    // Enable the Reconnect Button
    $reconBtn.removeAttr("disabled");
    // Disable the Disconnect Button
    $disconBtn.attr("disabled", "disabled");
}

function decryptInfo(data) {
    
    // decode the data from the payload string, then post it to the console and the Info bar
    var decodedText = atob(data);
    console.log(decodedText);
    return decodedText;
}